(function($) {
    function weather() {

        var _weather_desc, time, _day, _date,
            _month, _min_temp, _current_temp,
            _feels_like_temp, _max_temp, _time1,
            _time2, _time3, _city, _icon, _icon1,
            _icon2, _icon3,

            input_city_zip, _enabled_input,
            _selection_criteria, _is_city,
            _search, save_btn, _base_url

        _city_field = $("#id_city");
        _time_field = $("#id_time");
        _temp_max_field = $("#id_max_temp");
        _temp_min_field = $("#id_min_temp");
        _day_date_field = $("#id_day_date");
        _weather_desc_field = $("#id_weather_desc");

        _weather_icon_field = $("#id_weather_icon");

        _first_hour_icon_field = $("#id_upcoming_first_icon");
        _second_hour_icon_field = $("#id_upcoming_second_icon");
        _third_hour_icon_field = $("#id_upcoming_third_icon");

        _first_hour_field = $("#id_upcoming_first_temp");
        _second_hour_field = $("#id_upcoming_second_temp");
        _third_hour_field = $("#id_upcoming_third_temp");

        ajaxResponse = null;

        this.init = function() {

            //_base_url = "local.json";
            this._base_url = "http://api.wunderground.com/api/86cfae19cf9029ab/conditions/q/";

            this.input_city_zip = $('input:radio[name="city-zip"]');
            this.save_btn = $('#id_save');

            this._search = 'id_city_radio';
            if (this._search !== '') {
                $('#id_city').val(this._search);
            }

            $("#zip_group").hide();

            /* VIMP VIMP VIMP VIMP use proxy to pass the context of 'weather' 
            object for 'this' otherwise it will take 'this' keyword as a reference for DOM Button Save*/
            this.input_city_zip.change($.proxy(this._show_hide, this));
            this.save_btn.click($.proxy(this._city_zip, this));

            var d = new Date();
            this._time = d.getHours() + ":" + d.getMinutes();
            this._date = d.getDate();
            this._day = this._get_day_from_number(d.getDay());
            this._month = d.getMonth();



        }
    };

    weather.prototype._show_hide = function(event) {
        $("#zip_group").toggle();
        $("#city_group").toggle();
    }

    weather.prototype._fetch_data = function(_search) {
        var final_call_url = this._construct_url(this._base_url, this._search);
        this._get_json(final_call_url);
    }

    weather.prototype._city_zip = function() {
        this._enabled_input = $('input[type="radio"][name="city-zip"]:checked').attr('id');
        if (this._enabled_input == 'id_city_radio') {
            this._selection_criteria = 'id_city_radio';
            this._search = $('#id_txt_city').val();
        } else if (this._enabled_input == 'id_zip_radio') {
            this._selection_criteria = 'id_zip_radio';
            this._search = $('#id_txt_zip').val();
        }


        $("#id_preferences_form").submit();
        this._fetch_data(this._search);
    }

    weather.prototype._construct_url = function(base, param) {
        return (base + "IN/" + param + ".json");
    }

    weather.prototype._get_time = function() {
        alert("_get_time");
    }

    weather.prototype._get_json = function(final_url) {

        $.getJSON("local.json", function(result) {}).done(function(obj) {

            ajaxResponse = obj;

            this._time1 = $.now() + 1;
            this._time2 = $.now() + 2;
            this._time3 = $.now() + 3;

        });

        that = this;
        setTimeout(function() {
            that._get_date();
        }, 100);

    }

    weather.prototype._get_day_from_number = function(dayIndex) {
        return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"][dayIndex];
    }

    weather.prototype._get_date = function() {
        this._feels_like_temp = ajaxResponse.current_observation.feelslike_c;
        this._current_temp = ajaxResponse.current_observation.temp_c;
        this._weather_desc = ajaxResponse.current_observation.weather;
        this._city = ajaxResponse.current_observation.display_location.city;

        console.log(this._city + "yahoooo");
        console.log(this._feels_like_temp);
        console.log(this._current_temp);
        console.log(this._weather_desc);
        console.log(this._time);
        console.log(this._day);
        console.log(this._month);
    }

    weather.prototype._parse_json = function() {
        alert("_parse_json");
    }

    weather.prototype._set_city = function() {
        alert("_set_city");
    }

    weather.prototype._get_city = function() {
        alert("_get_city");
    }

    weather.prototype._get_icons = function() {
        alert("_get_icons");
    }

    weather.prototype._set_icons = function() {
        alert("_set_icons");
    }

    var current_weather = new weather();

    current_weather.init();

})(jQuery)